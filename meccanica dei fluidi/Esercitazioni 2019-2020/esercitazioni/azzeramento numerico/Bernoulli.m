function [u] = Bernoulli(u0)

global rho mi M h H D K90 g

%Eq. di Bernoulli modificata con perdite di carico
%Da azzerare per ricavare u

u = g*H-(u0^2)/2*(1+0.0791*mi^0.25/(rho^0.25*u0^0.25*D^0.25)*4*(M+2*h+H)/D+2*K90);
end

